lazy val root = (project in file(".")).
	settings(
		inThisBuild(List(
			organization := "robopig",
			scalaVersion := "2.12.1",
			version      := "0.1"
		)),
		name := "App",
		libraryDependencies += "org.scalatest" %% "scalatest" % "3.0.1",
		libraryDependencies += "org.scalamock" %% "scalamock-scalatest-support" % "3.5.0" % Test,
		mainClass in assembly := Some("robopig.App"),
		test in assembly := {}
	)
