package robopig.view;


import robopig.model.Model;
import robopig.model.player.InteractivePlayer;
import robopig.model.action.Action;

import robopig.util.Position;
import robopig.util.direction.*;
import robopig.util.event.*;
import robopig.util.event.Event;
import robopig.util.field.*;
import robopig.util.difficulty.*;

import java.util.LinkedList;
import java.util.List;

import scala.collection.JavaConverters;
import scala.collection.mutable.Publisher;

import javax.swing.*;

import java.awt.event.WindowEvent;
import java.awt.GridLayout;
import java.awt.Dimension;


/**
 *
 */
public class InteractiveView extends JFrame implements View {

    private Model model;
    private InteractivePlayer player;

    private JLabel[][] grid;

    public InteractiveView(Model model, InteractivePlayer player) {
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        this.model = model;
        this.player = player;
    }

    @Override
    public void showView() {
        String[] buttons = { "Easy", "Normal", "Hard" };

        int returnValue = JOptionPane.showOptionDialog(null, "Select the difficulty.", "Robopig Duel",
                JOptionPane.DEFAULT_OPTION, JOptionPane.PLAIN_MESSAGE,
                null, buttons, buttons[0]);

        switch (returnValue) {
            case 0: model.newGame(Easy$.MODULE$); break;
            case 1: model.newGame(Normal$.MODULE$); break;
            case 2: model.newGame(Hard$.MODULE$); break;
            default: dispatchEvent(new WindowEvent(this, WindowEvent.WINDOW_CLOSING));
        }
    }

    @Override
    public void notify(Publisher<Event> pub, Event event) {
        if (event instanceof NewGameEvent) {
            handleEvent((NewGameEvent)event);
        } else if (event instanceof FieldUpdateEvent) {
            handleEvent((FieldUpdateEvent)event);
        } else if (event instanceof PlayerMovedEvent) {
            handleEvent((PlayerMovedEvent)event);
        } else if (event instanceof GameOverEvent) {
            handleEvent((GameOverEvent)event);
        } else if (event instanceof GetActionsEvent) {
            handleEvent((GetActionsEvent)event);
        }
    }

    private void handleEvent(NewGameEvent event) {
        createGrid(event.width(), event.height());
        List<Field> fields = JavaConverters.seqAsJavaList(event.fields());
        for (int i = 0; i < event.width(); i++) {
            for (int j = 0; j < event.height(); j++) {
                updateField(new Position(i, j), fields.get(j + event.height() * i));
            }
        }
        setVisible(true);
        pack();
    }

    private void handleEvent(FieldUpdateEvent event) {
        updateField(event.pos(), event.value());
    }

    private void handleEvent(PlayerMovedEvent event) {
        if (event.player() == player) {
            updateCurrentPlayer(event.player().getPosition(), event.player().getDirection());
        } else {
            updateEnemyPlayer(event.player().getPosition(), event.player().getDirection());
        }
    }

    private void handleEvent(GameOverEvent event) {
        if (event.winner() == player){
            JOptionPane.showMessageDialog(null, "You won.");
        } else {
            JOptionPane.showMessageDialog(null, "You lost.");
        }
        dispatchEvent(new WindowEvent(this, WindowEvent.WINDOW_CLOSING));
    }

    private void handleEvent(GetActionsEvent event) {
        List<Action> actionList = JavaConverters.seqAsJavaList(player.getPossibleActions());
        Action[] actionArray = actionList.toArray(new Action[actionList.size()]);

        List<Action> actions = new LinkedList<>();

        for (int i = 0; i < event.num(); i++) {
            actions.add(selectAction(actionArray, i + 1, event.num()));
        }

        player.setCurrentActions(JavaConverters.asScalaBuffer(actions).toList());
    }

    private void createGrid(Integer width, Integer height) {
        GridLayout experimentLayout = new GridLayout(width, width);
        JPanel compsToExperiment = new JPanel();
        compsToExperiment.setLayout(experimentLayout);

        grid = new JLabel[height][width];
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                grid[i][j] = new JLabel();
                grid[i][j].setMinimumSize(new Dimension(ResourceHandler.ImageWidth, ResourceHandler.ImageHeight));
                grid[i][j].setMaximumSize(new Dimension(ResourceHandler.ImageWidth, ResourceHandler.ImageHeight));
                grid[i][j].setPreferredSize(new Dimension(ResourceHandler.ImageWidth, ResourceHandler.ImageHeight));
                compsToExperiment.add(grid[i][j]);
            }
        }

        add(compsToExperiment);
    }

    private Action selectAction(Action[] actions, Integer current, Integer all) {
        Action input = (Action)JOptionPane.showInputDialog(null, "Choose an action from the list (" + current + "/" + all + ").",
                "Robopig Duel", JOptionPane.NO_OPTION, null,
                actions,
                actions[0]);
        if (input == null) {
            dispatchEvent(new WindowEvent(this, WindowEvent.WINDOW_CLOSING));
        }
        return input;
    }

    private void setField(Position pos, Icon icon) {
        grid[pos.getX()][pos.getY()].setIcon(icon);
    }

    private void updateField(Position pos, Field field) {
        if (field instanceof Empty$) {
            setField(pos, ResourceHandler.GRASS);
        } else if (field instanceof Wall$) {
            setField(pos, ResourceHandler.WALL);
        }
    }

    private void updateCurrentPlayer(Position pos, Direction dir) {
        if (dir instanceof North$) {
            setField(pos, ResourceHandler.CURRENT_PLAYER_UP);
        } else if (dir instanceof South$) {
            setField(pos, ResourceHandler.CURRENT_PLAYER_DOWN);
        } else if (dir instanceof East$) {
            setField(pos, ResourceHandler.CURRENT_PLAYER_RIGHT);
        } else if (dir instanceof West$) {
            setField(pos, ResourceHandler.CURRENT_PLAYER_LEFT);
        }
    }

    private void updateEnemyPlayer(Position pos, Direction dir) {
        if (dir instanceof North$) {
            setField(pos, ResourceHandler.ENEMY_PLAYER_UP);
        } else if (dir instanceof South$) {
            setField(pos, ResourceHandler.ENEMY_PLAYER_DOWN);
        } else if (dir instanceof East$) {
            setField(pos, ResourceHandler.ENEMY_PLAYER_RIGHT);
        } else if (dir instanceof West$) {
            setField(pos, ResourceHandler.ENEMY_PLAYER_LEFT);
        }
    }
}
