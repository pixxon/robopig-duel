package robopig.view;


import javax.swing.ImageIcon;
import java.awt.Image;


class ResourceHandler {
    static Integer ImageWidth = 60;
    static Integer ImageHeight = 60;

    static ImageIcon GRASS = new ImageIcon(new ImageIcon("grass.png").getImage().getScaledInstance(ImageWidth, ImageHeight, Image.SCALE_DEFAULT));
    static ImageIcon WALL = new ImageIcon(new ImageIcon("wall.jpg").getImage().getScaledInstance(ImageWidth, ImageHeight, Image.SCALE_DEFAULT));

    static ImageIcon CURRENT_PLAYER_UP = new ImageIcon(new ImageIcon("current_player_up.png").getImage().getScaledInstance(ImageWidth, ImageHeight, Image.SCALE_DEFAULT));
    static ImageIcon CURRENT_PLAYER_DOWN = new ImageIcon(new ImageIcon("current_player_down.png").getImage().getScaledInstance(ImageWidth, ImageHeight, Image.SCALE_DEFAULT));
    static ImageIcon CURRENT_PLAYER_LEFT = new ImageIcon(new ImageIcon("current_player_left.png").getImage().getScaledInstance(ImageWidth, ImageHeight, Image.SCALE_DEFAULT));
    static ImageIcon CURRENT_PLAYER_RIGHT = new ImageIcon(new ImageIcon("current_player_right.png").getImage().getScaledInstance(ImageWidth, ImageHeight, Image.SCALE_DEFAULT));

    static ImageIcon ENEMY_PLAYER_UP = new ImageIcon(new ImageIcon("current_player_up.png").getImage().getScaledInstance(ImageWidth, ImageHeight, Image.SCALE_DEFAULT));
    static ImageIcon ENEMY_PLAYER_DOWN = new ImageIcon(new ImageIcon("current_player_down.png").getImage().getScaledInstance(ImageWidth, ImageHeight, Image.SCALE_DEFAULT));
    static ImageIcon ENEMY_PLAYER_LEFT = new ImageIcon(new ImageIcon("current_player_left.png").getImage().getScaledInstance(ImageWidth, ImageHeight, Image.SCALE_DEFAULT));
    static ImageIcon ENEMY_PLAYER_RIGHT = new ImageIcon(new ImageIcon("current_player_right.png").getImage().getScaledInstance(ImageWidth, ImageHeight, Image.SCALE_DEFAULT));
}
