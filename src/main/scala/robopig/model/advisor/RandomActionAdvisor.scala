package robopig.model.advisor

import robopig.model.action.Action
import robopig.model.player.Player
import robopig.persistence.board.GameBoard

import scala.util.Random

/**
  * The artificial intelligence of the game.
  *
  * Returns actions randomly.
  */
class RandomActionAdvisor extends ActionAdvisor {
  /**
    * Random generator to select actions.
    */
  private val rand = Random

  /**
    * Selects actions for the player randomly.
    *
    * @param board Current state of the game.
    * @param player The player who asks for advice.
    * @param actions List of possible actions.
    * @param num Number of wished actions.
    * @return List of selected actions.
    */
  override def getBestActions(board : GameBoard, player : Player, actions : List[Action], num : Int) : List[Action] = {
    assert(actions != Nil || num == 0)

    for (_ <- (0 until num).toList)
      yield actions(rand.nextInt(actions.length))
  }
}
