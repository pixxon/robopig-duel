package robopig.model.advisor

import robopig.model.action.Action
import robopig.model.player.Player
import robopig.persistence.board.GameBoard

/**
  * The artificial intelligence of the game.
  *
  * The artificial player uses this to determine it's next actions.
  */
trait ActionAdvisor {
  /**
    * Selects actions for the player.
    *
    * @param board Current state of the game.
    * @param player The player who asks for advice.
    * @param actions List of possible actions.
    * @param num Number of wished actions.
    * @return List of selected actions.
    */
  def getBestActions(board : GameBoard, player : Player, actions : List[Action], num : Int) : List[Action]
}
