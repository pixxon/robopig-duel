package robopig.model

import robopig.util.difficulty.Difficulty
import robopig.util.event.Event

import scala.collection.mutable.Publisher
import scala.collection.mutable.Subscriber

/**
  * The class is responsible for the logic of the game.
  */
trait Model extends Publisher[Event] with Subscriber[Event, Publisher[Event]] {
  /**
    * Starts a new game, which runs new rounds until game ends.
    *
    * @param diff The wished difficulty of the game.
    */
  def newGame(diff : Difficulty) : Unit
}
