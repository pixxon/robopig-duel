package robopig.model.action


import robopig.model.player.Player

import robopig.persistence.board.GameBoard

import robopig.util.Position


/**
  * Hits everybody around the player.
  */
class HitAction(private val player : Player) extends Action {
  /**
    * Executes the action, hitting everybody else around.
    *
    * @param board   The current board.
    * @param players The list of players.
    */
  override def execute(board : GameBoard, players : List[Player]): Unit = {
    for (p <- players if p.getHealth() != 0) {
      if (Position.getDistance(p.getPosition(), player.getPosition()) == 1) {
        p.setHealth(p.getHealth() - 1)
        if (p.getHealth() == 0) {
          board.setField(p.getPosition(), board.getField(p.getPosition()))
        }
      }
    }
  }

  override def toString : String = {
    "Hit"
  }

  override def getPlayer(): Player = {
    player
  }
}
