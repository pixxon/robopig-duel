package robopig.model.action


import robopig.model.player.Player
import robopig.persistence.board.GameBoard
import robopig.util.Position
import robopig.util.field.Wall


/**
  * Shoots a laser in players direction
  */
class ShootAction(private val player : Player) extends Action {
  /**
    * Executes the action, hitting everybody else around.
    *
    * @param board   The current board.
    * @param players The list of players.
    */
  override def execute(board : GameBoard, players : List[Player]): Unit = {
    var pos = Position.getNeighbour(player.getPosition(), player.getDirection())
    while (board.inRange(pos) && board.getField(pos) != Wall) {
      for (p <- players if p.getHealth() != 0) {
        if (Position.getDistance(p.getPosition(), pos) == 0) {
          p.setHealth(p.getHealth() - 1)
          if (p.getHealth() == 0) {
            board.setField(p.getPosition(), board.getField(p.getPosition()))
          }
        }
      }
      pos = Position.getNeighbour(pos, player.getDirection())
    }
  }

  override def toString : String = {
    "Shoot"
  }

  override def getPlayer(): Player = {
    player
  }
}
