package robopig.model.action


import robopig.model.player.Player

import robopig.persistence.board.GameBoard

import robopig.util.direction.Direction

/**
  * Turns the player to the given direction.
  */
class TurnAction(private val player : Player, private val dir : Direction) extends Action {
  /**
    * Executes the action, turning the player.
    *
    * @param board   The current board.
    * @param players The list of players.
    */
  override def execute(board : GameBoard, players : List[Player]): Unit = {
    player.setDirection(dir)
  }

  override def toString : String = {
    "Turn to " + dir.toString
  }

  override def getPlayer(): Player = {
    player
  }
}
