package robopig.model.action

import robopig.model.player.Player
import robopig.persistence.board.GameBoard

/**
  * Provides easy usage of different actions with the Command pattern.
  */
trait Action {
  /**
    * Executes the action, changing the board and players.
    *
    * @param board The current board.
    * @param players The list of players.
    */
  def execute(board : GameBoard, players : List[Player]) : Unit

  /**
    * Getter for the executor of the action.
    *
    * @return The player.
    */
  def getPlayer() : Player
}
