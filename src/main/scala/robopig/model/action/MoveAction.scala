package robopig.model.action


import robopig.model.player.Player

import robopig.persistence.board.GameBoard

import robopig.util.Position
import robopig.util.direction.Direction
import robopig.util.field.Empty


/**
  * Action which moves a player in the given direction.
  */
class MoveAction(private val player : Player, private val dir : Direction) extends Action {
  /**
    * Executes the action, moving the player if possible.
    *
    * @param board   The current board.
    * @param players The list of players.
    */
  override def execute(board : GameBoard, players : List[Player]): Unit = {
    val newPos : Position = Position.getNeighbour(player.getPosition(), dir)
    if (board.inRange(newPos) && board.getField(newPos) == Empty && players.forall(p => p.getHealth() == 0 || p.getPosition() != newPos)) {
      board.setField(player.getPosition(), board.getField(player.getPosition()))
      player.setPosition(newPos)
    }
  }

  override def toString : String = {
    "Move to " + dir.toString
  }

  override def getPlayer(): Player = {
    player
  }
}
