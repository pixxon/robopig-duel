package robopig.model


import robopig.persistence.board.GameBoard
import robopig.persistence.dataaccess.DataAccess
import robopig.model.player.{ArtificialPlayer, Player}
import robopig.model.action._
import robopig.model.advisor.RandomActionAdvisor
import robopig.util.difficulty.Difficulty
import robopig.util.event.{Event, GameOverEvent, NewGameEvent}
import robopig.util.Position
import robopig.util.direction._

import scala.collection.mutable.Publisher


/**
  */
class GameModel(private val dataAccess : DataAccess, private val  ps : List[Player]) extends Model {
  var board : GameBoard = _
  var players : List[Player] = ps

  /**
    * Starts a new game, which runs new rounds until game ends.
    *
    * @param diff The wished difficulty of the game.
    */
  override def newGame(diff: Difficulty): Unit = {
    board = dataAccess.createBoard(diff)
    players = players ++ List(new ArtificialPlayer(new RandomActionAdvisor()))
    publish(NewGameEvent(board.getWidth, board.getHeight, board.getAllFields()))
    subScribeToEverything()
    initPlayers()
    startRound()
  }

  override def notify(pub: Publisher[Event], event: Event): Unit = {
    publish(event)
  }

  private def initPlayers() : Unit = {
    val startingPositions : List[Position] = List(
      new Position(1, 1),
      new Position(board.getHeight - 2, board.getWidth - 2),
      new Position(1, board.getWidth - 2),
      new Position(board.getHeight - 2, 1)
    )
    val startingDirections : List[Direction] = List(
      South,
      North,
      South,
      North
    )

    for ((player, position, direction) <- (players, startingPositions, startingDirections).zipped.toList) {
      val possibleActions : List[Action] = List(
        new MoveAction(player, North),
        new MoveAction(player, South),
        new MoveAction(player, West),
        new MoveAction(player, East),
        new TurnAction(player, North),
        new TurnAction(player, South),
        new TurnAction(player, West),
        new TurnAction(player, East),
        new HitAction(player),
        new ShootAction(player)
      )

      player.setHealth(3)
      player.setPosition(position)
      player.setDirection(direction)
      player.setPossibleActions(possibleActions)
    }
  }

  private def subScribeToEverything() : Unit = {
    board.subscribe(this)
    for(player <- players) {
      player.subscribe(this)
    }
  }

  private def startRound() : Unit = {
    if (players.count(p => p.getHealth() > 0) < 2) {
      endGame()
    } else {
      simulateRound()
      startRound()
    }
  }

  private def endGame() : Unit = {
    val winner : Player = players.find(p => p.getHealth() > 1).orNull
    publish(GameOverEvent(winner))
  }

  private def simulateRound() : Unit = {
    val actions : List[List[Action]] =
      for (player <- players if player.getHealth() > 0)
        yield player.getCurrentActions(1)

    for (action <- actions.transpose.flatten) {
      if (action.getPlayer().getHealth() != 0) {
        action.execute(board, players)
      }
    }
  }
}
