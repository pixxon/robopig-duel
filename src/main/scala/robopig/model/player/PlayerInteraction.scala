package robopig.model.player

import robopig.model.action.Action
import robopig.util.event.Event

import scala.collection.mutable.Publisher

/**
  * Interface for the view to handle user interaction.
  */
trait PlayerInteraction  extends Publisher[Event] {
  /**
    * Sets the current actions for the round.
    *
    * @param actions List of new available actions.
    */
  def setCurrentActions(actions : List[Action]) : Unit

  /**
    * Getter for possible actions.
    *
    * @return A list of possible actions.
    */
  def getPossibleActions() : List[Action]
}
