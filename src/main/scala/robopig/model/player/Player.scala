package robopig.model.player

import robopig.util.direction.Direction
import robopig.util.Position
import robopig.model.action.Action
import robopig.util.event.Event

import scala.collection.mutable.Publisher

/**
  * Interface for the model of the player.
  */
trait Player extends Publisher[Event] {
  /**
    * Sets the possible actions for the round.
    *
    * @param actions List of possible actions.
    */
  def setPossibleActions(actions : List[Action]) : Unit

  /**
    * Returns the number of selected actions for the round.
    *
    * @param num Number of needed actions.
    * @return List of selected actions.
    */
  def getCurrentActions(num : Int) : List[Action]

  /**
    * Getter for position.
    *
    * @return Returns the players current position.
    */
  def getPosition() : Position

  /**
    * Setter for position.
    *
    * @param pos New position of the player.
    */
  def setPosition(pos : Position) : Unit

  /**
    * Getter for health points.
    *
    * @return The current health points of the player.
    */
  def getHealth() : Int

  /**
    * Setter for health points.
    *
    * @param hp The new value of the player's health points.
    */
  def setHealth(hp : Int) : Unit

  /**
    * Getter for the facing of the player.
    *
    * @return Current direction of the player.
    */
  def getDirection() : Direction

  /**
    * Setter for the player's facing.
    *
    * @param dir The new direction of the player.
    */
  def setDirection(dir : Direction) : Unit
}
