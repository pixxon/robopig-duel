package robopig.model.player

import robopig.model.action.Action
import robopig.util.Position
import robopig.util.direction.Direction
import robopig.util.event.{GetActionsEvent, PlayerMovedEvent}

/**
  * A controllable player.
  */
class InteractivePlayer() extends Player with PlayerInteraction {
  /**
    * Current position of the player.
    */
  private var position : Position = _

  /**
    * Current health of the player.
    */
  private var health : Int = _

  /**
    * Current direction of the player.
    */
  private var direction : Direction = _

  /**
    * List of possible actions.
    */
  private var possibleActions : List[Action] = Nil

  /**
    * List of selected actions.
    */
  private var currentActions : List[Action] = Nil

  /**
    * Flag to signal that current actions are set.
    */
  private var changed : Boolean = false

  /**
    * Sets the possible actions for the round.
    *
    * @param actions List of possible actions.
    */
  override def setPossibleActions(actions : List[Action]) : Unit = {
    possibleActions = actions
  }

  /**
    * Returns the number of selected actions for the round.
    *
    * @param num Number of needed actions.
    * @return List of selected actions.
    */
  override def getCurrentActions(num : Int) : List[Action] = {
    changed = false
    publish(GetActionsEvent(num))
    while(!changed)
      Thread.sleep(50)
    currentActions
  }

  /**
    * Getter for position.
    *
    * @return Returns the players current position.
    */
  override def getPosition() : Position = position

  /**
    * Setter for position.
    *
    * @param pos New position of the player.
    */
  override def setPosition(pos : Position) : Unit = {
    position = pos
    publish(PlayerMovedEvent(this))
  }

  /**
    * Getter for health points.
    *
    * @return The current health points of the player.
    */
  override def getHealth() : Int = health

  /**
    * Setter for health points.
    *
    * @param hp The new value of the player's health points.
    */
  override def setHealth(hp : Int) : Unit = {
    health = hp
  }

  /**
    * Getter for the facing of the player.
    *
    * @return Current direction of the player.
    */
  override def getDirection() : Direction = direction

  /**
    * Setter for the player's facing.
    *
    * @param dir The new direction of the player.
    */
  override def setDirection(dir : Direction) : Unit = {
    direction = dir
    publish(PlayerMovedEvent(this))
  }

  /**
    * Sets the current actions for the round.
    *
    * @param actions List of new available actions.
    */
  override def setCurrentActions(actions : List[Action]) : Unit = {
    currentActions = actions
    changed = true
  }

  /**
    * Getter for possible actions.
    *
    * @return A list of possible actions.
    */
  override def getPossibleActions() : List[Action] = possibleActions
}
