package robopig.model.player

import robopig.model.action.Action
import robopig.model.advisor.ActionAdvisor
import robopig.persistence.board.{DefaultGameBoard, GameBoard}
import robopig.util.Position
import robopig.util.direction.Direction
import robopig.util.event.{Event, FieldUpdateEvent, NewGameEvent, PlayerMovedEvent}

import scala.collection.mutable.{Publisher, Subscriber}

/**
  * Artificial Intelligence which can give actions based on the advisor.
  */
class ArtificialPlayer(private val adv : ActionAdvisor) extends Player with Subscriber[Event, Publisher[Event]] {
  /**
    * Current position of the player.
    */
  private var position : Position = _

  /**
    * Current health of the player.
    */
  private var health : Int = _

  /**
    * Current direction of the player.
    */
  private var direction : Direction = _

  /**
    * List of possible actions.
    */
  private var possibleActions : List[Action] = Nil

  /**
    * Action advisor based on possible actions and state of game.
    */
  private val advisor : ActionAdvisor = adv

  /**
    * Board to store the current state of the game.
    */
  private var board : GameBoard = _

  /**
    * Sets the possible actions for the round.
    *
    * @param actions List of possible actions.
    */
  override def setPossibleActions(actions : List[Action]) : Unit = {
    possibleActions = actions
  }

  /**
    * Returns the number of selected actions for the round.
    *
    * @param num Number of needed actions.
    * @return List of selected actions.
    */
  override def getCurrentActions(num : Int) : List[Action] = {
    advisor.getBestActions(board, this, possibleActions, num)
  }

  /**
    * Getter for position.
    *
    * @return Returns the players current position.
    */
  override def getPosition() : Position = position

  /**
    * Setter for position.
    *
    * @param pos New position of the player.
    */
  override def setPosition(pos : Position) : Unit = {
    position = pos
    publish(PlayerMovedEvent(this))
  }

  /**
    * Getter for health points.
    *
    * @return The current health points of the player.
    */
  override def getHealth() : Int = health

  /**
    * Setter for health points.
    *
    * @param hp The new value of the player's health points.
    */
  override def setHealth(hp : Int) : Unit = {
    health = hp
  }

  /**
    * Getter for the facing of the player.
    *
    * @return Current direction of the player.
    */
  override def getDirection() : Direction = direction

  /**
    * Setter for the player's facing.
    *
    * @param dir The new direction of the player.
    */
  override def setDirection(dir : Direction) : Unit = {
    direction = dir
    publish(PlayerMovedEvent(this))
  }

  /**
    * Handles the events of the game.
    *
    * @param pub Event sender.
    * @param event Type of the event.
    */
  override def notify(pub: Publisher[Event], event: Event): Unit = {
    event match {
      case NewGameEvent(width, height, fields) => {
        board = new DefaultGameBoard(width, height)
        for (i <- 0 until width) {
          for (j <- 0 until height) {
            board.setField(new Position(i, j), fields(i * height + j))
          }
        }
      }
      case FieldUpdateEvent(pos, value) => board.setField(pos, value)
    }
  }
}
