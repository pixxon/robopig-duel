package robopig.persistence.dataaccess

import robopig.persistence.board.GameBoard
import robopig.util.difficulty.Difficulty

/**
  * Factory for [[GameBoard]].
  */
trait DataAccess {
  /**
    * Creates a new GameBoard object based on [[Difficulty]]
    *
    * @param diff The given [[Difficulty]] of the game.
    * @return A new [[GameBoard]] object.
    */
  def createBoard(diff : Difficulty) : GameBoard
}
