package robopig.persistence.dataaccess

import robopig.persistence.board.{DefaultGameBoard, GameBoard}
import robopig.util.Position
import robopig.util.difficulty.{Difficulty, Easy, Hard, Normal}
import robopig.util.field.Wall

/**
  * Default implementation of [[DataAccess]] based on [[DefaultGameBoard]]
  */
class DefaultDataAccess extends DataAccess {/**
  * Creates a new GameBoard object based on [[Difficulty]]
  *
  * @param diff The given [[Difficulty]] of the game.
  * @return A new [[GameBoard]] object.
  */
  override def createBoard(diff : Difficulty): GameBoard = setWalls(innerCreateBoard(diff))

  /**
    * Creates a new DefaultGameBoard object based on [[Difficulty]]
    *
    * @param diff The given [[Difficulty]] of the game.
    * @return A new [[GameBoard]] object.
    */
  private def innerCreateBoard(diff : Difficulty) : GameBoard = {
    diff match {
      case Easy => new DefaultGameBoard(7, 7)
      case Normal => new DefaultGameBoard(11, 11)
      case Hard => new DefaultGameBoard(15, 15)
    }
  }

  /**
    * Sets the edges of the [[GameBoard]] to [[Wall]]
    *
    * @param board The game board.
    * @return The modified game board.
    */
  private def setWalls(board : GameBoard) : GameBoard = {
    for (x <- 0 until board.getWidth) {
      board.setField(new Position(x, 0), Wall)
      board.setField(new Position(x, board.getHeight - 1), Wall)
    }
    for (y <- 0 until board.getHeight) {
      board.setField(new Position(0, y), Wall)
      board.setField(new Position(board.getWidth - 1, y), Wall)
    }
    board
  }
}
