package robopig.persistence.board

import robopig.util.Position
import robopig.util.event.Event
import robopig.util.field.Field

import scala.collection.mutable.Publisher

/**
  * The world of the game. A two dimensional finite space.
  */
trait GameBoard extends Publisher[Event] {
  /**
    * Getter for the width of the table.
    *
    * @return The number of tiles in a row.
    */
  def getWidth : Int

  /**
    * Getter for the height of the table.
    *
    * @return The number of tiles in a column.
    */
  def getHeight : Int

  /**
    * Getter for a [[Field]] in the table.
    *
    * @param pos [[Position]] of the tile.
    * @return The current value of the specified tile.
    */
  def getField(pos : Position) : Field

  /**
    * Setter for a [[Field]] in the table.
    *
    * @param pos [[Position]] of the tile.
    * @param field The new value of the tile.
    */
  def setField(pos : Position, field : Field) : Unit

  /**
    * Returns all fields starting in the top left corner, row after row.
    *
    * @return All the fields of the board.
    */
  def getAllFields() : List[Field]

  /**
    * Checks if the given position is on the board.
    *
    * @param pos The position to check.
    * @return True if the position is inside.
    */
  def inRange(pos : Position) : Boolean
}
