package robopig.persistence.board

import robopig.util.Position
import robopig.util.event.FieldUpdateEvent
import robopig.util.field.{Empty, Field}

/**
  * Default implementation of [[GameBoard]] with two-dimensional array
  */
class DefaultGameBoard(private val width : Int, private  val height : Int) extends GameBoard {
  private val data : Array[Array[Field]] = Array.tabulate(width, height)((x, y) => Empty)

  /**
    * Getter for the width of the table.
    *
    * @return The number of tiles in a row.
    */
  override def getWidth : Int = width

  /**
    * Getter for the height of the table.
    *
    * @return The number of tiles in a column.
    */
  override def getHeight : Int = height

  /**
    * Getter for a [[Field]] in the table.
    *
    * @param pos [[Position]] of the tile.
    * @return The current value of the specified tile.
    */
  override def getField(pos : Position) : Field = {
    assert(pos.getX() >= 0 && pos.getX() < width)
    assert(pos.getY() >= 0 && pos.getY() < height)

    data(pos.getX())(pos.getY())
  }

  /**
    * Setter for a [[Field]] in the table.
    *
    * @param pos [[Position]] of the tile.
    * @param field The new value of the tile.
    */
  override def setField(pos : Position, field : Field) : Unit = {
    assert(pos.getX() >= 0 && pos.getX() < width)
    assert(pos.getY() >= 0 && pos.getY() < height)

    data(pos.getX())(pos.getY()) = field
    publish(FieldUpdateEvent(pos, field))
  }

  /**
    * Returns all fields starting in the top left corner, row after row.
    *
    * @return All the fields of the board.
    */
  override def getAllFields(): List[Field] = {
    for (i <- (0 until width).toList; j <- (0 until height).toList)
        yield data(i)(j)
  }

  /**
    * Checks if the given position is on the board.
    *
    * @param pos The position to check.
    * @return True if the position is inside.
    */
  override def inRange(pos: Position): Boolean = {
    pos.getX() >= 0 && pos.getX() < height && pos.getY() >= 0 && pos.getY() < width
  }
}
