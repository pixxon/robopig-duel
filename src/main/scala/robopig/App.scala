package robopig

import robopig.model.{GameModel, Model}
import robopig.model.player.InteractivePlayer
import robopig.persistence.dataaccess.{DataAccess, DefaultDataAccess}
import robopig.view.{InteractiveView, View}

object App {
	def main(args: Array[String]): Unit = {
		val dataAccess : DataAccess = new DefaultDataAccess()
		val player : InteractivePlayer = new InteractivePlayer()
		val model : Model = new GameModel(dataAccess, List(player))
		val view : View = new InteractiveView(model, player)
		model.subscribe(view)

		view.showView()
	}
}
