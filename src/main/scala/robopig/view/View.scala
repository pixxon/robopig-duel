package robopig.view

import robopig.util.event.Event

import scala.collection.mutable.Subscriber
import scala.collection.mutable.Publisher

/**
  * Responsible for the UI and visualization.
  */
trait View extends Subscriber[Event, Publisher[Event]] {
  /**
    * Shows the user interfaces.
    */
  def showView() : Unit
}
