package robopig.util.difficulty

/**
  * Represents different game settings.
  *
  * More difficult games could mean stronger or more opponents.
  * It can also mean a bigger board or more complex actions.
  */
abstract sealed class Difficulty

/**
  * Defines the lowest difficulty level.
  *
  * The easiest AI, 1 opponent and a 10 by 10 board
  */
case object Easy extends Difficulty

/**
  * Defines the default difficulty level.
  *
  * The most difficulty AI, 2 opponents and a 21 by 21 board.
  */
case object Normal extends Difficulty

/**
  * Defines the hardest difficulty level.
  *
  * The most difficulty AI, 3 opponents and a 31 by 31 board.
  */
case object Hard extends Difficulty
