package robopig.util.field

/**
  * Creates an abstraction in the possible types of tiles.
  *
  * It creates an easy usability and expendable implementation.
  */
abstract sealed class Field

/**
  * Empty tile, anybody can step on it.
  */
case object Empty extends Field

/**
  * A tile containing a wall, nobody can step on it.
  */
case object Wall extends Field
