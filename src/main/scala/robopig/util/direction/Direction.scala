package robopig.util.direction

/**
  * Represents general directions in the game.
  *
  * They can be used to show the direction of an action or the facing of the player.
  */
abstract sealed class Direction

/**
  * North means upwards.
  */
case object North extends Direction

/**
  * West means downwards.
  */
case object West extends Direction

/**
  * South means left side.
  */
case object South extends Direction

/**
  * East means right side.
  */
case object East extends Direction
