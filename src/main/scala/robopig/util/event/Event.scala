package robopig.util.event

import robopig.model.player.Player
import robopig.util.Position
import robopig.util.field.Field

/**
  * Represents events occurring inside the game.
  *
  * These events can be used only within Subscriber and Publisher classes.
  */
abstract sealed class Event

/**
  * Signals the end of the game.
  *
  * @param winner The winning player.
  */
case class GameOverEvent(winner : Player) extends Event

/**
  * Signals that a tile changed it's value.
  *
  * @param pos Position of the tile.
  * @param value New value of the tile.
  */
case class FieldUpdateEvent(pos : Position, value : Field) extends Event

/**
  * Signals that it's the players turn to provide actions.
  *
  * @param num Number of needed actions.
  */
case class GetActionsEvent(num : Int) extends Event

/**
  * Signals that a new game started.
  *
  * @param width Width of the board.
  * @param height Height of the board.
  * @param fields List of all fields row by row.
  */
case class NewGameEvent(width : Int, height : Int, fields : List[Field]) extends Event

/**
  * Signals that a player moved.
  *
  * @param player The moving player.
  */
case class PlayerMovedEvent(player : Player) extends Event
