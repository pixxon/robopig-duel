package robopig.util

import robopig.util.direction._

/**
  * Represents a position in a two dimensional world.
  *
  * Positions are on grids, meaning coordinates are integers.
  */
class Position(private val x : Int, private val y : Int) {
  /**
    * Getter for the X coordinate.
    *
    * @return Returns the X coordinate.
    */
  def getX() : Int = x

  /**
    * Getter for the Y coordinate.
    *
    * @return Returns the Y coordinate.
    */
  def getY() : Int = y
}

object Position {
  /**
    * Finds the nearest neighbour in the given direciton.
    *
    * @param pos Current position.
    * @param dir Direction of search.
    * @return The neighbour.
    */
  def getNeighbour(pos : Position, dir : Direction) : Position = {
    dir match {
      case North => new Position(pos.x - 1, pos.y)
      case South => new Position(pos.x + 1, pos.y)
      case East => new Position(pos.x, pos.y + 1)
      case West => new Position(pos.x, pos.y - 1)
    }
  }

  /**
    * Calculates the distance between two points.
    *
    * @param pos1 First points.
    * @param pos2 Second points.
    * @return The rounded up distance of the points.
    */
  def getDistance(pos1 : Position, pos2 : Position) : Int = {
    val xDiff = pos1.x - pos2.x
    val yDiff = pos1.y - pos2.y
    Math.sqrt(xDiff * xDiff + yDiff * yDiff).toInt
  }
}
