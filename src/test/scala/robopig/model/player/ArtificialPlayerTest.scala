package robopig.model.player

import org.scalamock.scalatest.MockFactory
import org.scalatest.FlatSpec
import org.scalatest.Matchers._

import robopig.model.action.Action
import robopig.model.advisor.ActionAdvisor
import robopig.persistence.board.GameBoard
import robopig.util.Position
import robopig.util.direction.{Direction, North, South}

/**
  * Test class for [[ArtificialPlayer]]
  */
class ArtificialPlayerTest  extends FlatSpec with MockFactory {

  val mockAdvisor : ActionAdvisor = new ActionAdvisor {
    override def getBestActions(board : GameBoard, player : Player, actions : List[Action], num : Int) : List[Action] = {
      for (_ <- (0 until num).toList)
        yield actions.head
    }
  }

  val player: ArtificialPlayer = new ArtificialPlayer(mockAdvisor)

  "ArtificialPlayer" should "change it's position" in {
    val startingPosition : Position = new Position(0, 0)
    val newPosition: Position = new Position(4, 3)

    player.setPosition(startingPosition)
    player.getPosition() should be(startingPosition)
    player.setPosition(newPosition)
    player.getPosition() should be(newPosition)
  }

  it should "change it's direction" in {
    val startingDirection : Direction = North
    val newDirection: Direction = South

    player.setDirection(startingDirection)
    player.getDirection() should be(startingDirection)
    player.setDirection(newDirection)
    player.getDirection() should be(newDirection)
  }

  it should "change it's health" in {
    val startingHealth : Int = 5
    val newHealth: Int = 3

    player.setHealth(startingHealth)
    player.getHealth() should be(startingHealth)
    player.setHealth(newHealth)
    player.getHealth() should be(newHealth)
  }

  it should "return the given actions" in {
    val mockAction1 : Action = mock[Action]
    val mockAction2 : Action = mock[Action]
    val mockAction3 : Action = mock[Action]
    val mockActionList : List[Action] = List(mockAction1, mockAction2, mockAction3)

    player.setPossibleActions(mockActionList)
    player.getCurrentActions(2) should be(List(mockAction1, mockAction1))
    player.getCurrentActions(3) should be(List(mockAction1, mockAction1, mockAction1))
    player.getCurrentActions(4) should be(List(mockAction1, mockAction1, mockAction1, mockAction1))
  }
}
