package robopig.model.player

import org.scalamock.scalatest.MockFactory
import org.scalatest.FlatSpec
import org.scalatest.Matchers._
import robopig.model.action.Action
import robopig.util.Position
import robopig.util.direction.{Direction, North, South}
import robopig.util.event.{Event, GetActionsEvent}

import scala.collection.mutable
import scala.collection.mutable.{Publisher, Subscriber}

/**
  * Test class for [[InteractivePlayer]]
  */
class InteractivePlayerTest extends FlatSpec with MockFactory  {
  val player : InteractivePlayer = new InteractivePlayer()


  val mockSubscriber : Subscriber[Event, Publisher[Event]] = new Subscriber[Event, Publisher[Event]] {
    override def notify(pub : mutable.Publisher[Event], event : Event) : Unit = {
      event match {
        case GetActionsEvent(num) => player.setCurrentActions(player.getPossibleActions().take(num))
      }
    }
  }

  "InteractivePlayer" should "change it's position" in {
    val startingPosition : Position = new Position(0, 0)
    val newPosition: Position = new Position(4, 3)

    player.setPosition(startingPosition)
    player.getPosition() should be(startingPosition)
    player.setPosition(newPosition)
    player.getPosition() should be(newPosition)
  }

  it should "change it's direction" in {
    val startingDirection : Direction = North
    val newDirection: Direction = South

    player.setDirection(startingDirection)
    player.getDirection() should be(startingDirection)
    player.setDirection(newDirection)
    player.getDirection() should be(newDirection)
  }

  it should "change it's health" in {
    val startingHealth : Int = 5
    val newHealth: Int = 3

    player.setHealth(startingHealth)
    player.getHealth() should be(startingHealth)
    player.setHealth(newHealth)
    player.getHealth() should be(newHealth)
  }

  it should "return the given actions" in {
    val mockAction1 : Action = mock[Action]
    val mockAction2 : Action = mock[Action]
    val mockAction3 : Action = mock[Action]
    val mockAction4 : Action = mock[Action]
    val mockActionList : List[Action] = List(mockAction1, mockAction2, mockAction3, mockAction4)

    player.setPossibleActions(mockActionList)
    player.subscribe(mockSubscriber)
    player.getCurrentActions(2) should be(List(mockAction1, mockAction2))
    player.getCurrentActions(3) should be(List(mockAction1, mockAction2, mockAction3))
    player.getCurrentActions(4) should be(List(mockAction1, mockAction2, mockAction3, mockAction4))
  }
}
