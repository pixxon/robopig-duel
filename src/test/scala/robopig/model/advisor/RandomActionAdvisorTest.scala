package robopig.model.advisor

import org.scalatest.FlatSpec
import org.scalatest.Matchers._
import org.scalatest.Inspectors._
import org.scalamock.scalatest.MockFactory

import robopig.model.action.Action
import robopig.model.player.Player
import robopig.persistence.board.GameBoard

/**
  * Test class for [[RandomActionAdvisor]]
  */
class RandomActionAdvisorTest extends FlatSpec with MockFactory {
  val advisor : ActionAdvisor = new RandomActionAdvisor
  val mockBoard : GameBoard = mock[GameBoard]
  val mockPlayer : Player = mock[Player]
  val mockAction1 : Action = mock[Action]
  val mockAction2 : Action = mock[Action]
  val mockAction3 : Action = mock[Action]
  val mockActionList : List[Action] = List(mockAction1, mockAction2, mockAction3)

  "ActionAdvisor" should "raise AssertionError" in {
    an [AssertionError] should be thrownBy advisor.getBestActions(mockBoard, mockPlayer, Nil, 5)
  }

  it should "return empty list" in {
    advisor.getBestActions(mockBoard, mockPlayer, mockActionList, 0) should be(Nil)
  }

  it should "give defined number of actions" in {
    advisor.getBestActions(mockBoard, mockPlayer, mockActionList, 5).length should be(5)
    advisor.getBestActions(mockBoard, mockPlayer, mockActionList, 2).length should be(2)
    advisor.getBestActions(mockBoard, mockPlayer, mockActionList, 6).length should be(6)
    advisor.getBestActions(mockBoard, mockPlayer, mockActionList, 7).length should be(7)
    advisor.getBestActions(mockBoard, mockPlayer, mockActionList, 2).length should be(2)
  }

  it should "give actions only from given list" in {
    forAll(advisor.getBestActions(mockBoard, mockPlayer, mockActionList, 3)) {
      x => mockActionList should contain(x)
    }

    forAll(advisor.getBestActions(mockBoard, mockPlayer, mockActionList, 8)) {
      x => mockActionList should contain(x)
    }
  }
}
