package robopig.persistence.dataaccess

import org.scalatest.FlatSpec
import org.scalatest.Matchers._
import robopig.persistence.board.GameBoard
import robopig.util.Position
import robopig.util.difficulty.{Easy, Hard, Normal}
import robopig.util.field.{Empty, Wall}

/**
  * Test class for [[DefaultDataAccess]].
  */
class DefaultDataAccessTest extends FlatSpec {
  val dataAccess : DefaultDataAccess = new DefaultDataAccess

  val easyTable : GameBoard = dataAccess.createBoard(Easy)
  val normalTable : GameBoard = dataAccess.createBoard(Normal)
  val hardTable : GameBoard = dataAccess.createBoard(Hard)

  "DefaultGameBoard" should "create table for every difficulty" in {
    easyTable should not be null
    normalTable should not be null
    hardTable should not be null
  }

  it should "create tables with specified sizes" in {
    easyTable.getHeight should be(7)
    easyTable.getWidth should be(7)

    normalTable.getHeight should be(11)
    normalTable.getWidth should be(11)

    hardTable.getHeight should be(15)
    hardTable.getWidth should be(15)
  }

  it should "place walls on edges" in {
    for (x <- 0 until easyTable.getWidth; y <- 0 until easyTable.getHeight)
      if (x == 0 || x == easyTable.getWidth - 1 || y == 0 || y == easyTable.getHeight - 1)
        easyTable.getField(new Position(x, y)) should be(Wall)
      else
        easyTable.getField(new Position(x, y)) should be(Empty)

    for (x <- 0 until normalTable.getWidth; y <- 0 until normalTable.getHeight)
      if (x == 0 || x == normalTable.getWidth - 1 || y == 0 || y == normalTable.getHeight - 1)
        normalTable.getField(new Position(x, y)) should be(Wall)
      else
        normalTable.getField(new Position(x, y)) should be(Empty)

    for (x <- 0 until hardTable.getWidth; y <- 0 until hardTable.getHeight)
      if (x == 0 || x == hardTable.getWidth - 1 || y == 0 || y == hardTable.getHeight - 1)
        hardTable.getField(new Position(x, y)) should be(Wall)
      else
        hardTable.getField(new Position(x, y)) should be(Empty)
  }
}
