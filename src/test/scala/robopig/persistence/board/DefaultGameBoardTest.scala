package robopig.persistence.board

import org.scalatest.FlatSpec
import org.scalatest.Matchers._
import robopig.util.Position
import robopig.util.field.{Empty, Wall}

/**
  * Test class for [[DefaultGameBoard]]
  */
class DefaultGameBoardTest extends FlatSpec {
  val testBoard : GameBoard = new DefaultGameBoard(13, 15)

  "DefaultGameBoard" should "initialize to Empty" in {
    for (x <- 0 until testBoard.getWidth; y <- 0 until testBoard.getHeight)
      testBoard.getField(new Position(x, y)) should be(Empty)
  }

  it should "have given dimensions" in {
    testBoard.getWidth should be(13)
    testBoard.getHeight should be(15)
  }

  it should "raise assert when index out of range in getField" in {
    an [AssertionError] should be thrownBy testBoard.getField(new Position(-1, 0))
    an [AssertionError] should be thrownBy testBoard.getField(new Position(testBoard.getWidth, 0))
    an [AssertionError] should be thrownBy testBoard.getField(new Position(0, -1))
    an [AssertionError] should be thrownBy testBoard.getField(new Position(0, testBoard.getHeight))
  }

  it should "raise assert when index out of range in setField" in {
    an [AssertionError] should be thrownBy testBoard.setField(new Position(-1, 0), Wall)
    an [AssertionError] should be thrownBy testBoard.setField(new Position(testBoard.getWidth, 0), Wall)
    an [AssertionError] should be thrownBy testBoard.setField(new Position(0, -1), Wall)
    an [AssertionError] should be thrownBy testBoard.setField(new Position(0, testBoard.getHeight), Wall)
  }

  it should "change it's value at given position" in {
    val pos : Position = new Position(testBoard.getWidth / 2, testBoard.getHeight / 2)

    testBoard.getField(pos) should be(Empty)
    testBoard.setField(pos, Wall)
    testBoard.getField(pos) should be(Wall)
  }

  it should "return all the fields" in {
    testBoard.getAllFields().length should be(13 * 15)
  }
}
